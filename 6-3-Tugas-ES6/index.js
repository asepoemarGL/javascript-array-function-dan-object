let batasSoal = () => { console.log ("========================================") }

//Soal 1
console.log("Jawaban Soal 1\n")

console.log("Persegi Panjang:\npanjang = 18 & lebar = 12\n")

let luas = (panjang, lebar) => {

    let l = panjang * lebar

    return l
}
console.log("Luas = ",luas(18,12))

const keliling = (p , l) => {
    const k = 2 * (p+l)

    return k
}
console.log("Keliling = ",keliling(18,12),"\n")


batasSoal()
//Soal 2
console.log("Jawaban Soal 2\n")

// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//       }
//     }
//   }
//Driver Code 
// newFunction("William", "Imoh").fullName()

// const newFunction = (firstName, lastName) => {
//     console.log(firstName + " " + lastName)
// }

const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
      }
    }
  }

newFunction("William", "Imoh").fullName()
console.log("\n")


batasSoal()
//Soal 3
console.log("Jawaban Soal 3\n")

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby,"\n")

batasSoal()
//Soal 4
console.log("Jawaban Soal 4\n")


const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined,"\n")


batasSoal()
//Soal 5
console.log("Jawaban Soal 5\n")

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

let after = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet}`
console.log(before)
console.log(after)

console.log("\n ----------DONE----------")

