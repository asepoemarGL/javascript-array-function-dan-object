function batasSoal() {
    console.log ("========================================")
}

//Soal 1
console.log("Jawaban Soal 1\n")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
console.log(daftarHewan,"\n-menjadi:")

daftarHewan.sort()
let text = ""
let len = daftarHewan.length

for (let i = 0 ; i < len; i++) {
  text += daftarHewan[i] + " \n"
}

console.log(text)
batasSoal()

//Soal 2
console.log("Jawaban Soal 2\n")

function introduce(isi) {
    // return {
    //     "Nama Saya" : isi.name,
    //     "Umur Saya" : isi.age,
    //     "alamat saya di" : isi.address, 
    //     "dan saya punya hobby yaitu" : isi.hobby
    // }

    return "Nama saya " + [isi.name] + ", umur saya "+[isi.age]+" tahun, alamat saya di "+[isi.address]+", dan saya punya hobby yaitu "+[isi.hobby]+"!"
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan,"\n")

batasSoal()

//Soal 3
console.log("Jawaban Soal 3\n")


function hitung_huruf_vokal(string) {
    var hurufVokal = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
    let counts = 0;
    for(let i = 0; i < hurufVokal.length; i++) {
        if(hurufVokal.includes(string[i])) {
        counts++;
        }
    }

    return counts

}
//https://www.w3schools.com/jsref/jsref_includes.asp

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2,"\n") // 3 2

batasSoal()

//Soal 4
console.log("Jawaban Soal 4\n")

function hitung(angka) {

    var a = angka + angka - 2
    return a

}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

console.log("\n -----DONE-----")