var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

let timeTotal = books.reduce((prev, next) => {
    return prev + next.timeSpent;
  }, 0);
  
// console.log('Total Time:', timeTotal);

readBooksPromise(timeTotal, books[0])
    .then(
        (res) => {
            return readBooksPromise(res, books[1])
        }
    )
    .then(
        (res) => {
            return readBooksPromise(res, books[2])
        }
    )
    .then(
        (res) => {
            return readBooksPromise(res, books[3])
        }
    )
    // .catch(error => console.log(error))
    .catch((res) => {
        console.log(`Selesai, sisa waktu adalah ${res}`)
    })


