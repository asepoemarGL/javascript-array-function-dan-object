// di index.js
var readBooks = require('./callback.js')

let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

let timeTotal = books.reduce((prev, next) => {
    return prev + next.timeSpent;
}, 0);
//https://sebhastian.com/javascript-sum-array-objects/
  
// console.log('Total Time:', timeTotal);

// readBooks(books[0], timeTotal, function(sisaWaktu){
//     readBooks(books[1], sisaWaktu, function(sisaWaktu){
//         readBooks(books[2], sisaWaktu, function(sisaWaktu){
//             readBooks(books[3], sisaWaktu, function(time){})
//         })
//     })
// })

readBooks(books[0], timeTotal, (sisaWaktu)=>{
    readBooks(books[1], sisaWaktu, (sisaWaktu) => {
        readBooks(books[2], sisaWaktu, (sisaWaktu) => {
            readBooks(books[3], sisaWaktu, (time) => { })
        })
    })
})


